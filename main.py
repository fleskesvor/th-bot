#!/usr/bin/python
"""
This script handles all imports and all the nasty wiring of dependencies,
so that user scripts can be as simple as possible.
"""
import os
from copy import copy
from optparse import OptionParser, Option, OptionValueError

from config import USER, PASS
from thbot.session import init, login
# Initialize session and set globals.
receive_event, send_event, stats = init()
from thbot.chat import Chat
from thbot.adventure import Adventure
from thbot.messages import Messages
from thbot.types import Effect

def main():
    parser = OptionParser(option_class=MyOption)
    parser.add_option("-s", "--script", dest="script", type="file",
                      help="run user script FILE", metavar="FILE")
    #parser.add_option("-c", "--config", dest="config",
    #                  help="use config file FILE", metavar="FILE",
    #                  default="config.py")
    # options for username and password, which override config
    
    (options, args) = parser.parse_args()

    """
    Catch interruptions to give users a chance to wrap things up or recover.
    """
    try:
        # If errors, possibly catch them unless verbose option is set.
        # execfile(options.script)
        #execfile("scripts/messages.py", globals())
        #execfile("scripts/adventure.py", globals())
        execfile("scripts/chat.py", globals())
        #execfile("scripts/stats.py", globals())
        # TODO: Make init() only return send_event()
        send_event("script.started")
    except KeyboardInterrupt:
        send_event("keyboard.interrupt")

def check_file(option, opt, value):
    """
    TODO: Make sure file is a valid file under this directory.
    """
    # Also try execfile()
    if not os.path.isfile(value):
        raise OptionValueError(
            "option %s: invalid script file: %r" % (opt, value)
        )
    return value
        
class MyOption(Option):
    """
    Sub class Option class to implement file type checker
    """
    TYPES = Option.TYPES + ("file",)
    TYPE_CHECKER = copy(Option.TYPE_CHECKER)
    TYPE_CHECKER["file"] = check_file
    
if __name__ == "__main__":
    main()
