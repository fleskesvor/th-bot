# th-bot
This is a re-implementation of a bot I made in Perl almost 10 years ago, which interacts with the casual browser-based MMORPG [Twilight Heroes](http://www.twilightheroes.com).

The main purpose of the project is to learn best practices for web scraping with Python, and the bot may never become feature complete.

It should be noted that such "community bots" are endorsed by the creators of games like Twilight Heroes and the similar [Kingdom of Loathing](http://www.kingdomofloathing.com).

**Requirements**

This bot relies on the packages [Requests](http://docs.python-requests.org/en/master/) and [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/):

```
pip install bs4
pip install requests
```

If you prefer to use other modules for web scraping, like [Mechanize](http://wwwsearch.sourceforge.net/mechanize/), you may want to install these two modules in an isolated Python environment with [Virtualenv](https://virtualenv.pypa.io/).

**TODO**

* Add optional filter to decorator to separate handling on certain attributes, like message types and encounter names.
* Catch ConnectionError and other connection issues, e.g. rollover.
* Give user script authors an easy way to reconnect.
* Make sure the adventure parser is generic enough to handle every possible input. Hidden input especially shouldn't be something the user has to deal with.
* Parse loot at the end of every adventure.
* Identify all encounters if possible.
