"""
Stat monitoring for an adventure bot.
"""
from bs4 import BeautifulSoup as bs
from re import compile as re_compile, sub

#from thbot.session import session, send_event
from config import URL
from types import Effect

session = None
send_event = None

NOT_NUMBER = re_compile(r"\D")

class Stats(object):
    """
    Parse vital information that an adventurer might need.
    """
    def __init__(self, _session=None, _send_event=None):
        """
        Parse stats.
        """        
        if not _session and not _send_event:
            raise ImportError("Stats() must be accessed though global 'stats'")

        global session, send_event
        session = _session
        send_event = _send_event
        
        self._parse()

    def refresh(self):
        """
        Call this when fresh stats are needed.
        """
        self._parse()
        # Send everything plus a few convenience variables?
        send_event("stats.refreshed")

    def _parse(self):
        """
        I'm not sure how this will be used yet, but maybe just
        private functions and access variables directly.
        """        
        response = session.get(URL + "/nav.php")

        soup = bs(response.text, 'html.parser')

        #self.hp = _get_hp(soup)
        hp = _get_hp(soup)
        self.hp = Stat("hp", hp)
        #self.pp = _get_pp(soup)
        pp = _get_pp(soup)
        self.pp = Stat("pp", pp)
        self.chips = _get_chips(soup)
        self.effects = _get_effects(soup)
        self.turns_remaining = self.turns_left = _get_turns_left(soup)

        scraps = _get_scraps(soup)
        #self.strength, self.intellect, self.reflexes = _get_stats(scraps)
        strength, intellect, reflexes = _get_stats(scraps)
        self.strength = Stat(strength)
        self.intellect = Stat(intellect)
        self.reflexes = Stat("reflexes", reflexes)

        self.reputation = _get_reputation(scraps)
        self.level = _get_level(scraps)
        self.xp = _get_xp(scraps)

        # TODO: Separate events for chips/items
        send_event("stats.changed", self)

def _get_level(scraps):
    for scrap in scraps:
        if scrap.startswith("Level "):
            level = scrap.split(" ")[1]
            if not level:
                level = '0'
            return int(level)
    return None

def _get_xp(scraps):
    for scrap in scraps:
        if scrap.startswith("XP: "):
            xp = scrap.split(" ")[1]
            return int(sub(NOT_NUMBER, "", xp))
    return None

def _get_reputation(scraps):
    for scrap in scraps:
        if scrap.startswith("Reputation: "):
            return scrap.split(" ")[1]
    return None

def _get_stats(scraps):
    strength, intellect, reflexes = None, None, None
    for scrap in scraps:
        if ": " not in scrap or " (" not in scrap:
            continue
        stat, value = scrap[:-1].split(": ")
        current, base = value.split(" (")
        stats = (int(current), int(base))
        if stat == "Strength":
            strength = stats
        elif stat == "Intellect":
            intellect = stats
        elif stat == "Reflexes":
            reflexes = stats
    return strength, intellect, reflexes

def _get_scraps(soup):
    """
    Hack out the rest of the stuff we might be interested in.
    """
    strongs = soup.find_all('strong')
    scraps = []

    for strong in strongs:
        br = strong.parent.find("br")
        if not br:
            continue
        br.extract()
        scraps.append(strong.parent.text)
    return scraps

def _get_turns_left(soup):
    """
    TODO: differenciate betweeen no turns and failed to parse.
    """
    fonts = soup.find_all('font', attrs={'class': 'smnav'})
    for font in fonts:
        text = font.text
        if text and text.endswith("left)"):
            return sub(NOT_NUMBER, "", text)
    return None

def _get_hp(soup):
    """
    What if hp is > 999?
    """
    hp_str = soup.find(id="hpstring")
    hp_cur, hp_max = hp_str.text.split("/")
    if not hp_cur:
        hp_cur = '0'
    if not hp_max:
        hp_max = '0'
    return int(hp_cur), int(hp_max)

def _get_pp(soup):
    """
    What if pp is > 999?
    """
    pp_str = soup.find(id="ppstring")
    pp_cur, pp_max = pp_str.text.split("/")
    if not pp_cur:
        pp_cur = '0'
    if not pp_max:
        pp_max = '0'
    return int(pp_cur), int(pp_max)

def _get_chips(soup):
    chips_str = soup.find(id="chips")
    chips = chips_str.text.replace(",", "")
    return int(chips)

def _get_effects(soup):
    effects = []
    effect_types = {
        "showsk": Effect.sidekick,
        "showskill": Effect.skill,
        "showitem": Effect.item
    }
    navs = soup.find_all('font', attrs={'class': 'smnav'})
    for nav in navs:
        container = nav.find(['div', 'a'])
        if container and container.has_attr('onclick'):
            effect_name = container.text
            if effect_name.endswith(" min"):
                last_index = effect_name.rfind(" -")
                effect_name = effect_name[0:last_index]
            click_str = container.get('onclick')
            effect_type, effect_id = click_str[:-2].split("(")
            effects.append({
                "id": effect_id,
                "name": effect_name,
                "type": effect_types[effect_type]
            })
    return effects

class Stat(object):
    """
    Send event when stats change
    """
    def __init__(self, *args):
        """
        Rename to current and base/max stats.
        Should also receive name of stat
        """
        self.name = None
        if args and isinstance(args[0], basestring):
            self.name = args[0]
            args = args[1:]
        self.current = 0
        self.max = self.base = 0
        self.update(*args)
        """
        Trigger an event when stat changes
        """
        
    def update(self, *args):
        if not args:
            return False
        """
        TODO: If stats actually change, return True
        """
        if isinstance(args[0], tuple):
            self.current = args[0][0]
            self.max = self.base = args[0][1]        
        elif len(args) == 2:
            self.current = args[0]
            self.max = self.base = args[0]
        if self.name:
            event_name = "%s.changed" % self.name
            send_event(event_name, self.current, self.max, self._percentage())
        
    def __lt__(self, comparator):
        comparable, comparator = self._convert(comparator)
        return comparable < comparator
        
    def ___le__(self, comparator):
        comparable, comparator = self._convert(comparator)
        return comparable <= comparator

    def __eq__(self, comparator):
        comparable, comparator = self._convert(comparator)
        return comparable == comparator
        
    def __ne__(self, comparator):
        comparable, comparator = self._convert(comparator)
        return comparable != comparator
        
    def __gt__(self, comparator):
        comparable, comparator = self._convert(comparator)
        return comparable > comparator

    def __ge__(self, comparator):
        comparable, comparator = self._convert(comparator)
        return comparable >= comparator

    def _convert(self, comparator):
        """
        Return comparable and comparator suitable for comparison.
        """
        if isinstance(comparator, (int, long)):
            return self.current, comparator
        elif isinstance(comparator, float) and comparator >= 0:
            return self._fraction(), comparator
        elif isinstance(comparator, basestring) and comparator.endswith('%'):
            # TODO: Check if this is working as intended.
            return self._fraction(), float(comparator[:-1])
        else:
            raise ValueError("Comparator must be a whole number or fraction")

    def _fraction(self):
        if not self.max:
            return 0
        return float(self.current) / float(self.max)

    def _percentage(self):
        return self._fraction() * 100

    def __str__(self):
        return "%d/%d (%.0f%%)" % (self.current, self.max, self._percentage())

