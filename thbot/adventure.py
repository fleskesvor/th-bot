#!/usr/bin/python
import requests, json
from bs4 import BeautifulSoup as bs

from thbot.session import session, send_event
from config import URL

"""
TODO: Consider calling listeners with named arguments instead of positional
      ones to be consequent between different modules.
"""

class Adventure(object):
    """
    Handle sending and reading of messages through chat.
    """
    def __init__(self, max_rounds=30):
        """
        Keep track of which round we're at.
        """
        self.round = 0
        self.max_rounds = max_rounds
      
    def fight(self, payload):
        """
        * location is only set at the beginning of a fight
        * choice is set during combat (attack/skill/item/run)
        * pickwhich is set if attack with skill/item
        * if attack, hpstring+ppstring is sent
        """
        if "location" in payload:
            response = session.get(URL + "/fight.php",
                                   params=payload)
            self.round = 1            
        else:
            response = session.post(URL + "/fight.php",
                                    data=payload)
            self.round += 1

        #print response.text

        soup = bs(response.text, 'html.parser')        
        combat, encounter, default_actions, all_actions = _parse(response.text)

        if not default_actions:
            # Combat summary has chip gains instead of encounter currently
            chips, items, xp = _parse_result(soup)
            send_event("adventure.ended", encounter, chips, items, xp, response.text)
            return

        payload = default_actions
        # TODO: Make something better than ACTIONS in config.py
        #if encounter in ACTIONS:
        #    payload.update(ACTIONS[encounter])

        # Also pass self.rounds to encounter and combat listeners
        user_actions = send_event("adventure.encounter", combat, encounter, self.round, default_actions, all_actions, response.text)
        if combat:
            combat_actions = send_event("adventure.combat", encounter, self.round, default_actions, all_actions, response.text)
            if combat_actions:
                user_actions = combat_actions
        else:
            choice_actions = send_event("adventure.choice", encounter, default_actions, all_actions, response.text)
            if choice_actions:
                user_actions = choice_actions            
        
        #user_actions = self.callback(combat, encounter, default_actions, all_actions, response.text)
        if user_actions and isinstance(user_actions, dict):
            payload.update(user_actions)
            
        return self.fight(payload)

def _parse(content):
    soup = bs(content, 'html.parser')
    html_forms = soup.find_all('form')
    combat, encounter = _parse_encounter(soup)
    all_actions = []
    
    for form in html_forms:
        html_inputs = form.find_all('input')
        html_selects = form.find_all('select')
        action, inputs = None, {}
        
        for input in html_inputs:
            _type, name, value, text = _parse_input(input)
            #print _type, name, value, text
            if _type == "submit":
                action = value
                continue
            if not name in inputs:
                inputs[name] = []                
            inputs[name].append({"value": value, "text": text})

        for select in html_selects:
            html_options = select.find_all('option')
            _type, name = "option", select.get("name")
            for option in html_options:
                value = option.get("value")
                if not value:
                    continue
                text = option.text
                #print _type, name, value, text
                if not name in inputs:
                    inputs[name] = []
                inputs[name].append({"value": value, "text": text})

        all_actions.append({"action": action, "inputs": inputs})

    default_actions = _get_default_actions(all_actions)

    return combat, encounter, default_actions, all_actions

def _get_default_actions(all_actions):
    default_actions = {}
    if len(all_actions) < 1:
        return None
    for key in all_actions[0]["inputs"]:
        value = all_actions[0]["inputs"][key][0]["value"]
        default_actions[key] = value
    return default_actions

def _parse_encounter(soup):
    h1, h2 = soup.h1, soup.h2
    if h2:
        return False, h2.text.replace("\n", "")
    b = soup.find({"b": {"value": "enemy"}})
    if not b:
        return None, h1.text.replace("\n", "")
    return True, b.text.replace("\n", "")
    
def _parse_input(input):
    _type = input.get("type")
    name = input.get("name")
    value = input.get("value")
    text = input.find(text=True, recursive=False)
    if text:
        text = text.strip().replace("\n", "")
    if not text:
        text = None
    return _type, name, value, text

# What's this for?
def _parse_radio(input, name, value):
    text = input.find(text=True)
    text = text.replace("\n", "")
    return {"payload": {name: value}, "text" : text}

def _parse_result(soup):
    """
    This doesn't handle every eventuality, I think.
    Result tag has total XP, but not int, str, ref stats.
    """
    chips, xp = 0, 0
    result_tag = soup.find("span", {"id": "result"})
    if result_tag:
        chips = int(result_tag.get("data-chips"))
        xp = int(result_tag.get("data-xp"))
    items = _parse_items(soup)
    return chips, items, xp
    
def _parse_items(soup):
    items = []
    item_tags = soup.find_all("b", {"id": "drop"})
    for tag in item_tags:
        name = tag.text.strip()
        items.append(name)
    return items
