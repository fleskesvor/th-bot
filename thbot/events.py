"""
Use decorators to enable users to receive pre-defined events.
A payload can be sent back to functions that might want user
input, like chat or adventures.
"""
_functions = {}

def receive_event(name):
    """
    Decorator to be applied to user made functions to enable
    them to add as much logic as they want without having to
    deal with any of the dirty underpinnings.
    """
    def decorator(function):
        if name not in _functions:
            _functions[name] = []
        _functions[name].append(function)
        #def wrapper(self=None, *args, **kwargs):
        def wrapper(*args, **kwargs):
            function(*args, **kwargs)
        return wrapper
    return decorator

def send_event(name, *args, **kwargs):
    """
    Payload can potentially come from more than one event
    listeners, but we have to pick one.    
    """
    if not name in _functions:
        return
    payload = None
    for func in _functions[name]:
        _payload = func(*args, **kwargs)
        if _payload is not None:
            payload = _payload
    return payload

