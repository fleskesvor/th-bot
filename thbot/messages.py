#!/usr/bin/python
"""
This module should contain everything one might need to run a
buff bot or similar, but can be expanded to support sending of
messages and other related features.

Do I really need separate get() and refresh()? Will user ever
want to have a "cached" list of messages?

TODO: Make message parsing cleaner and more robust.
"""
from bs4 import BeautifulSoup as bs
from time import sleep

from thbot.session import session, send_event
from config import URL

class Messages(object):
    """
    Fetch all messages as a list of message dicts.

    I don't think there's a reason for this to auto-fetch.

    Will send one event with all messages when started
    and one for every message read (that is newer than
    specified time, or received after login)

    messages.loaded
    message.received

    Should have a timeout. Maybe 60 seconds is a reasonable value?
    """
    def __init__(self, timeout=60):
        self.timeout = timeout
        self.shutdown = False
        self.messages = []

        # Last as in newest
        self._last_message_id = 0

        self.run()

    def get(self):
        self.run()
        self.shutdown = True
        return self.messages

    def run(self):
        """
        load() is the new get()

        Should probably rebuild object (take optional datestamp too?)
        Datestamp could be date string or just "now"/"yesterday" (enums?)
        """

        while not self.shutdown:

            messages = []
            response = session.get(URL + "/messages-inbox.php")
            soup = bs(response.text, 'html.parser')
            cells = soup.find_all(_is_message)
            
            for cell in cells[::-1]:
                message = Message(self, cell)

                # Skip previously seen messages
                if self._last_message_id >= message.message_id:
                    continue
                self._last_message_id = message.message_id

                messages.append(message)
                self._last_message = message

                send_event("message.received", message)

                # Just for testing purposes
                #break

            send_event("messages.loaded", messages)
            self.messages.extend(messages)
            
            if not self.shutdown:
                sleep(self.timeout)

    def refresh(self):
        self.messages = []
        self._delete_messages = {}
        return self.get()

    def delete(self, message_id):
        print "Delete message %s from collection" % message_id
        print "%d messages before deletion" % len(self.messages)
        index, found = 0, False
        for message in self.messages:
            if message.message_id == message_id:
                found = True
                break
            index += 1
        if found:
            message = self.messages[index]
            print message_id
            print message.sender_name
            print message.text
            del message
            del self.messages[index]
            print "%d messages after deletion" % len(self.messages)

    def __del__(self):
        """
        Message object might need help with garbage collection.
        """
        for message in self.messages:
            del message
        del self.messages

class Message(object):
    def __init__(self, collection, soup):
        self.collection = collection
        self.html = str(soup)

        links = soup.find_all('a')
        message_id, delete_link = _get_id_and_delete_link(links)
        item = _get_item(soup)
        text, chips = _get_text_and_chips(soup)
        date_string = _get_date_string(soup)
        sender_id, sender_name = _get_sender(links)

        self.message_id = message_id
        self.sender_id = sender_id
        self.sender_name = sender_name
        self.text = text
        self.item = item
        self.chips = chips
        self.date_string = date_string

        self._delete_link = delete_link

    def delete(self):
        print "Delete message %s" % self.message_id
        self.collection.delete(self.message_id)

    def __del__(self):
        """
        Message objects are created for all messages in inbox every time
        messages are parsed, and already seen messages will get discarded
        immediately and garbage collected.
        """
        del self.collection
    
# Various helper functions

def _is_message(cell):
    if cell and cell.has_attr('bgcolor'):
        return len(cell.find_all('strong')) > 1
    return False

def _get_text_and_chips(cell):
    """
    Use the number of <strong> elements to determine
    if chips or not to avoid cheating.
    """
    strong = cell.find_all('strong')
    # Remove the message element to make it easier to get date.
    ul = cell.ul.extract()
    text = " ".join(ul.find_all(text=True))
    chips = 0
    if len(strong) > 2:
        text, chips = text.split("You were sent: ")
        chips = chips.strip().split(" ")[0]
        chips = int(chips.replace(",", ""))
    return text, chips

def _get_item(cell):
    item_row = cell.find('tr', attrs={'valign': 'middle'})
    if item_row:
        item_row.extract()
        item_str = item_row.find('b').text[:-1]
        name, quantity = item_str.split(" (quantity ")
        return (name, int(quantity))
    return None

def _get_sender(links):
    sender_name = links[0].text
    sender_id = links[0]["href"].split("=")[1]
    return int(sender_id), sender_name

def _get_id_and_delete_link(links):
    delete_link = links[2]["href"]
    start = delete_link.find("=")
    end = delete_link.find("&")
    message_id = delete_link[start+1:end]
    return int(message_id), delete_link

def _get_date_string(cell):
    """
    This must be called after text has been extracted.
    """
    return cell.text.split("Date: ")[1]
