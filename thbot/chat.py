"""
A simple foundation for a chat bot.
"""
import requests, json
from time import sleep

from thbot.session import session, receive_event, send_event
from config import URL

class Chat(object):
    """
    Handle sending and reading of messages through chat.
    """
    def __init__(self, timeout=2):
        """
        We need the logged in session and a callback function
        """
        self.timeout = timeout
        self.last_id = -1
        self.shutdown = False
        self.seconds = 0
        # Messages is probably a better name than commands
        self.commands = []

    #@receive_event("chat.send")
    def post(self, commands):
        """
        For this to be called from the outside,
        run() should be run in a separate thread.
        """
        if commands is None:
            return
        if isinstance(commands, bool) and not commands:
            self.stop()
        if isinstance(commands, basestring):
            self.commands.append(commands)
        if isinstance(commands, list):
            self.commands.extend(commands)

    def stop(self):
        self.shutdown = True

    def run(self):
        """
        Catch SystemExit as well?
        """
        try:
            self._run()
        except KeyboardInterrupt:
            send_event("chat.exit", "keyboard interrupt")
            self.stop()
        
    def _run(self):
        headers = {'connection': 'keep-alive'}
        
        while not self.shutdown:
            response = None

            commands = send_event("chat.poll", self.seconds)
            self.seconds += self.timeout
            self.post(commands)                
            
            # TODO: Handle ConnectionError
            # Is there a reason why we only pop one command?
            if len(self.commands) > 0:
                command = self.commands.pop(0)
                payload = {'last': self.last_id, 'text': command}
                response = session.post(URL + "/chat/chat_get.php",
                                        headers=headers, data=payload)
            else:
                payload = {'last': self.last_id}
                response = session.get(URL + "/chat/chat_get.php",
                                       headers=headers, params=payload)

            obj = json.loads(response.text)
            if not isinstance(obj, dict) and not isinstance(obj, list):
                continue
            
            if "messages" in obj:
                for message in obj["messages"]:
                    arguments = {}

                    if "type" in message:
                        arguments["_type"] = message["type"]
                    else:
                        arguments["_type"] = "normal"
                    for key in ("text", "channel", "user", "link"):
                        arguments[key] = message.get(key, None)

                    commands = send_event("chat.receive", message, **arguments)
                    self.post(commands)

                    if "id" in message:
                        self.last_id = int(message["id"])
                        
            if not self.shutdown:
                sleep(self.timeout)
                
    def __del__(self):
        """
        This is probably redundant.
        """
        self.stop()
