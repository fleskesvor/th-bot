#!/usr/bin/python
"""
Access point for session related functions and globals.
"""
import requests
from bs4 import BeautifulSoup as bs

from config import URL
from thbot.events import receive_event as _receive, send_event as _send
from thbot.stats import Stats

# Shared requests.session
session = None
# Cross-module globals
receive_event, send_event = None, None
stats = None

def init():
    """
    Share session object and event functions between modules.
    """
    global session, receive_event, send_event, stats
    session = requests.Session()
    receive_event, send_event = _receive, _send
    stats = Stats(session, send_event)
    return receive_event, send_event, stats

def _post_login():
    """
    When user has been logged in, we must make sure that global objects like
    stats and locations have been populated before user can access them.
    """
    # TODO: Consider mute flag to allow initialization without causing
    # a bunch of events to be triggered.
    stats.refresh()

def login(name, password):
    """
    Try to log in and return True if successful.
    """
    payload = {'name': name, 'pass': password}

    response = session.post(URL + "/verify.php", data=payload)
    soup = bs(response.text, 'html.parser')

    # For now, use Javascript redirect to check if we're logged in or not.
    if soup.script and soup.script.string.startswith("top.location"):
        location = soup.script.string.split("'")[1]
        if location == "index2.php":
            _post_login()
            send_event("login.success", "Logged in!")
            return True, "Logged in!"
        # We get a redirect, but no specific error message.
        # Make sure you spelled your user name correctly in config.py.
        send_event("login.failure", "Login failed.")
        return False, "Login failed."

    # Print error message from server.
    send_event("login.failure", soup.body.text)
    return False, soup.body.text
