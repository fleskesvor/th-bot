"""
Read messages.
"""

"""
Generic listeners
"""
@receive_event("script.started")
def main():
    login(USER, PASS)

@receive_event("login.success")
def logged_in(message):
    print message
    Messages()
        
@receive_event("messages.loaded")
def messages_loaded(messages):
    print "Loaded %d messages..." % len(messages)

@receive_event("message.received")
def message_received(message):
    print """
    Message Id\t%s
    Received\t%s
    Message\t%s
    Chips\t%s
    Item\t%s
    """ % (message.message_id, message.date_string,
           message.text, message.chips, message.item)
    print message.html

@receive_event("message.received")
def handle_message(message):
    print
    print message.sender_name, "\t", message.chips
    print
    if message.sender_name == "flesk" and message.chips == 1:
        message.delete()
