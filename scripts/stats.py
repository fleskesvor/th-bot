"""
Get player character's stats.
"""

"""
Generic listeners
"""
@receive_event("script.started")
def main():
    login(USER, PASS)

@receive_event("login.success")
def logged_in(message):
    """
    TODO: Make better examples of use-cases.
    """
        
    # Pretty print HP stats
    print "HP: ", stats.hp
    
    # Print current HP
    print "Current HP: ", stats.hp.current
    
    # Print current/max PP
    pp = stats.pp
    print "Current/max PP: %d/%d" % (pp.current, pp.max)
    
    # Chips
    print "Chips: ", stats.chips
    
    # Turns left
    print "Turns remaining: ", stats.turns_remaining
    
    # Reputation
    print "Reputation: ", stats.reputation
    
    # Level
    print "Level: ", stats.level
    
    # XP
    print "XP: ", stats.xp
    
    # Pretty print strength/intellect/reflexes
    print "Strength: ", stats.strength
    print "Intellect: ", stats.intellect
    print "Reflexes: ", stats.reflexes
    
    # Print a list of current effects
    print stats.effects
    
    # Filter by effect type (must import Effect from types module)
    print [eff for eff in stats.effects if eff["type"] == Effect.skill]

"""
Stat specific listeners
"""
@receive_event("hp.changed")
def hp_changed(current, max, percentage):
    print "HP changed -> %d/%d (%f%%)" % (current, max, percentage)

@receive_event("pp.changed")
def pp_changed(current, max, percentage):
    print "PP changed -> %d/%d (%f%%)" % (current, max, percentage)

@receive_event("reflexes.changed")
def reflexes_changed(current, base, percentage):
    print "Reflexes changed -> %d/%d (%f%%)" % (current, base, percentage)
    
@receive_event("stats.changed")
def stats_changed(stats):
    print "Stats, chips or items lost or gained"
    print "HP: ", stats.hp
    print "Reflexes: ", stats.reflexes
