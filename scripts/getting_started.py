"""
Log onto Twilight Heroes and then leave.
"""

"""
Generic listeners
"""
@receive_event("script.started")
def start():
    """
    Login details can be set in config.py for convenience,
    but can be input as quoted text instead, in which case
    the import of config above is not needed. E.g.:
    login("flesk", "test123")
    """
    login(USER, PASS)

@receive_event("login.success")
def logged_in(message):
    """
    This is where we define which modules we want to use
    throughout our script. Custom made functionality can
    be called from here.
    """
    print message
    
@receive_event("login.failure")
def not_logged_in(message):
    """
    This event is triggered if a login attempt failed,
    and can be used to trigger a new login attempt. It is,
    however, completely optional to listen for this event.
    """
    print message
