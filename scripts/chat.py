"""
Interact with the chat.
"""

"""
Generic listeners
"""
@receive_event("script.started")
def main():
    login(USER, PASS)

@receive_event("login.success")
def logged_in(message):
    print message
    chat = Chat()
    chat.run()
    
@receive_event("login.failure")
def not_logged_in(message):
    print message

"""
Chat specific listeners
"""
@receive_event("chat.poll")
def chat_poller(seconds):
    """
    Example poller for actions not triggered by a message.
    """
    print "Polling chat..."

    if seconds == 4:
        # See who's in chat
        return "/who"
    if seconds > 6:
        # Exit chat
        return False

@receive_event("chat.receive")
def pretty_print(message, _type, text, channel, user, link):
    """
    Pretty print messages from chat.
    """
    if _type in ["normal", "em"]:
        print "[%-8s] %s (#%s): %s" % (channel, user, link, text)
    elif _type == "note":
        print "! %s" % text
    elif _type == "pm":
        print "[~private] %s (#%s): %s" % (user, link, text)

@receive_event("chat.receive")
def chat_bot(message, _type, user, **kwargs):
    """
    Respond to private messages.
    """
    if _type == "pm":
        return "/msg %s Hi!" % user
