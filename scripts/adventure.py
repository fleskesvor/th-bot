"""
Go on an adventure.
"""

"""
Generic listeners
"""
@receive_event("script.started")
def main():
    login(USER, PASS)

@receive_event("keyboard.interrupt")
def aborted():
    print "Script killed by user..."
    
@receive_event("login.success")
def logged_in(message):
    print message
    adv = Adventure()
    # TODO: Build number of adventures into module
    # TODO: Change name of function fight()
    for _ in range(3):
        adv.fight({'location': 17})

@receive_event("adventure.ended")
def handle_non_adventure(encounter, chips, items, xp, html):
    # TODO: This gets called every time an adventure ends,
    # so it's more of a summary and should be a way to
    # list chip/item/stat gains, etc.
    
    """
    Use this function to examine why the adventure ended, and
    take actions to start new adventures, if wanted.
    """
    print "No adventure: ", encounter
    print chips, items, xp
    print "HTML: ", html
    
@receive_event("adventure.choice")
def handle_choice(*args):
    print "CHOICE: ", args[2]
    
@receive_event("adventure.combat")
def handle_combat(enemy, _round, defaults, actions, html):
    print "ENEMY, round %d: %s" % (_round, enemy)
    
@receive_event("adventure.encounter")
def handle_encounters(combat, encounter, _round, defaults, actions, html):
    """
    This is supposed to be easy to extend for anyone, but we're not
    quite there yet.
    Script will go into endless recursion if anything is wrong with
    the payload (or it is empty), so implement mechanisms to handle
    that, like something depending on the round of combat. Throw
    error if something seems badly configured by the user.
    """
    payload = {}

    """
    Pre-defined choices can be set in a per-user configuration that
    will have easy read/write access both from text editors and
    user scripts.
    """
    if not combat:
        print encounter,"- default actions:", defaults
        print "available actions:", actions
        for key in defaults:
            print "select value for",
            value = raw_input(key + ": ")
            payload[key] = value
            print payload

    return payload
